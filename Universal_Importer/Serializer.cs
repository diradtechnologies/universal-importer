﻿using System.IO;
using System.Xml.Serialization;

public class Serializer
{
    public void SerializeObject(string filename, SignatureObject _sig)
    {
        XmlSerializer xmlSerializer = new XmlSerializer(typeof(SignatureObject));
        TextWriter textWriter = (TextWriter)new StreamWriter(filename);
        xmlSerializer.Serialize(textWriter, (object)_sig);
        textWriter.Close();
    }

    public SignatureObject DeSerializeObject(string filename)
    {
        XmlSerializer xmlSerializer = new XmlSerializer(typeof(SignatureObject));
        FileStream fileStream = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.Read);
        SignatureObject signatureObject = (SignatureObject)xmlSerializer.Deserialize((Stream)fileStream);
        fileStream.Close();
        return signatureObject;
    }
}
