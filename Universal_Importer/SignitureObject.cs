﻿using System;

[Serializable]
public class SignatureObject
{
    public string LinkingName = "";
    public string SigType = "CSV";
    public string[] list_Map;
    public string[] headers;
    public string[] onGoodEmails;
    public string[] onCriticalEmails;
    public string distinct = "";
    public string CSV_FILE_PATH_URL = "";
    public string WS_USERNAME = "";
    public string WS_PASSWORD = "";
    public int ArchiveFileNumDays = 7;
    public string local_db_server = "";
    public string local_db_user = "";
    public string local_db_pass = "";
    public string local_db_database = "";
    public string local_db_table = "";
    public bool local_db_use_windows_auth;
    public string source_dir = "";
    public string ftp_host = "";
    public string ftp_user = "";
    public string ftp_pass = "";
    public string ftp_working_dir = "";
    public string triggerDll = "C:\\Users\\kevin.garabedian.DIRAD\\Documents\\Visual Studio 2010\\Projects\\TriggerTest\\TriggerTest\\bin\\Release\\TriggerTest.dll";
    public string on_success_class = "trigger";
    public string on_success_method = "PayLoad";
}