﻿using System;
using System.Collections;
using System.Reflection;

namespace DiRAD.Universe
{
    public class DynaInvoke
    {
        public static Hashtable AssemblyReferences = new Hashtable();
        public static Hashtable ClassReferences = new Hashtable();

        public static object InvokeMethodSlow(
          string AssemblyName,
          string ClassName,
          string MethodName,
          object[] args)
        {
            foreach (Type type in Assembly.LoadFrom(AssemblyName).GetTypes())
            {
                if (type.IsClass && type.FullName.EndsWith("." + ClassName))
                {
                    object instance = Activator.CreateInstance(type);
                    return type.InvokeMember(MethodName, BindingFlags.InvokeMethod, (Binder)null, instance, args);
                }
            }
            throw new Exception("could not invoke method");
        }

        public static DynaInvoke.DynaClassInfo GetClassReference(
          string AssemblyName,
          string ClassName)
        {
            if (!DynaInvoke.ClassReferences.ContainsKey((object)AssemblyName))
            {
                Assembly assembly;
                if (!DynaInvoke.AssemblyReferences.ContainsKey((object)AssemblyName))
                    DynaInvoke.AssemblyReferences.Add((object)AssemblyName, (object)(assembly = Assembly.LoadFrom(AssemblyName)));
                else
                    assembly = (Assembly)DynaInvoke.AssemblyReferences[(object)AssemblyName];
                foreach (Type type in assembly.GetTypes())
                {
                    if (type.IsClass)
                    {
                        Console.WriteLine(ClassName);
                        if (type.FullName.EndsWith("." + ClassName))
                        {
                            DynaInvoke.DynaClassInfo dynaClassInfo = new DynaInvoke.DynaClassInfo(type, Activator.CreateInstance(type));
                            DynaInvoke.ClassReferences.Add((object)AssemblyName, (object)dynaClassInfo);
                            Console.WriteLine(ClassName);
                            return dynaClassInfo;
                        }
                    }
                }
                throw new Exception("could not instantiate class");
            }
            return (DynaInvoke.DynaClassInfo)DynaInvoke.ClassReferences[(object)AssemblyName];
        }

        public static object InvokeMethod(
          DynaInvoke.DynaClassInfo ci,
          string MethodName,
          object[] args)
        {
            return ci.type.InvokeMember(MethodName, BindingFlags.InvokeMethod, (Binder)null, ci.Classobject, args);
        }

        public static object InvokeMethod(
          string AssemblyName,
          string ClassName,
          string MethodName,
          object[] args)
        {
            return DynaInvoke.InvokeMethod(DynaInvoke.GetClassReference(AssemblyName, ClassName), MethodName, args);
        }

        public class DynaClassInfo
        {
            public Type type;
            public object Classobject;

            public DynaClassInfo()
            {
            }

            public DynaClassInfo(Type t, object c)
            {
                this.type = t;
                this.Classobject = c;
            }
        }
    }
}
