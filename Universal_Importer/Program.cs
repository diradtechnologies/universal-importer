﻿using LumenWorks.Framework.IO.Csv;
using Renci.SshNet;
using SendGridMail;
using SendGridMail.Transport;
using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using System.Net.FtpClient;
using System.Net.Mail;
using System.Net.Mime;
using System.Security.Cryptography;
using System.Text;


namespace DiRAD.Universe
{
    internal class Program
    {
        private string[] headers;
        private string[] list_Map;
        private string distinct = "";
        private Hashtable HTdistinct = new Hashtable();
        private static string ApplicationVersion = "2.0.1";

        private static int GetNumericHeader(string[] headers, string value)
        {
            for (int index = 0; index < headers.Length; ++index)
            {
                if (headers[index] == value)
                    return index;
            }
            return -1;
        }

        private static string simpleEncrypt(string input) => " dbo.EncryptString('" + input.Replace("'", "") + "') ";

        private static string convertNumeric(string Alpha = "")
        {
            Alpha = Alpha.ToLower();
            string str = "";
            for (int index = 0; index < Alpha.Length; ++index)
            {
                switch (Alpha[index])
                {
                    case '0':
                        str += "0";
                        break;
                    case '1':
                        str += "1";
                        break;
                    case '2':
                    case 'a':
                    case 'b':
                    case 'c':
                        str += "2";
                        break;
                    case '3':
                    case 'd':
                    case 'e':
                    case 'f':
                        str += "3";
                        break;
                    case '4':
                    case 'g':
                    case 'h':
                    case 'i':
                        str += "4";
                        break;
                    case '5':
                    case 'j':
                    case 'k':
                    case 'l':
                        str += "5";
                        break;
                    case '6':
                    case 'm':
                    case 'n':
                    case 'o':
                        str += "6";
                        break;
                    case '7':
                    case 'p':
                    case 'q':
                    case 'r':
                    case 's':
                        str += "7";
                        break;
                    case '8':
                    case 't':
                    case 'u':
                    case 'v':
                        str += "8";
                        break;
                    case '9':
                    case 'w':
                    case 'x':
                    case 'y':
                    case 'z':
                        str += "9";
                        break;
                }
            }
            return str;
        }

        private static string DownloadFile(SignatureObject _sig)
        {
            string str1 = "";
            try
            {
                string[] strArray1 = _sig.CSV_FILE_PATH_URL.Split(':');
                string str2 = "";
                string distinct = _sig.distinct;
                switch (strArray1[0].ToUpper())
                {
                    case "HTTP":
                        using (WebClient webClient = new WebClient())
                        {
                            webClient.Proxy = (IWebProxy)null;
                            str2 = webClient.DownloadString(_sig.CSV_FILE_PATH_URL);
                            break;
                        }
                    case "FILE":
                        string path1 = _sig.CSV_FILE_PATH_URL.ToLower().Replace("file:\\\\", "");
                        if (!System.IO.File.Exists(path1))
                            return "NO-FILE-EXIT|" + path1;
                        str2 = System.IO.File.ReadAllText(path1);
                        System.IO.File.Delete(path1);
                        break;
                    case "SFTP":
                        string[] strArray2 = strArray1[1].Substring(2).Split('/');
                        string host = strArray2[0];
                        string path2 = "/";
                        SftpClient sftpClient = new SftpClient(host, "dirad438", "DiRAD6000");
                        try
                        {
                            sftpClient.Connect();
                            Console.WriteLine("Connection was Successful");
                        }
                        catch (Exception ex)
                        {
                            Program.sendDebugEmail("Import Application Runtime Error", ex);
                            Console.Write(ex.Message);
                            return "ERROR|" + ex.Message;
                        }
                        using (sftpClient)
                        {
                            for (int index = 1; index < strArray2.Length - 1; ++index)
                                path2 = path2 + strArray2[index] + "/";
                            sftpClient.ChangeDirectory(path2);
                            string path3 = strArray2[strArray2.Length - 1];
                            string str3 = "C:\\importer\\";
                            if (sftpClient.Exists(path3))
                            {
                                using (FileStream fileStream = new FileStream("C:\\importer\\" + path3, FileMode.Create))
                                {
                                    try
                                    {
                                        System.IO.File.Exists(path3);
                                        sftpClient.DownloadFile(path3, (Stream)fileStream);
                                        sftpClient.DeleteFile(path3);
                                        System.IO.File.Exists(str3 + path3 + ".old");
                                    }
                                    catch (Exception ex)
                                    {
                                        if (ex.Message == "No such file")
                                        {
                                            if (sftpClient.IsConnected)
                                                sftpClient.Disconnect();
                                            return "ERROR-404|" + str3 + path3;
                                        }
                                        Program.sendDebugEmail("Import Application Runtime Error", ex);
                                    }
                                }
                                sftpClient.Disconnect();
                                str2 = System.IO.File.ReadAllText("C:\\Importer\\" + path3);
                                System.IO.File.Exists("C:\\Importer\\" + path3);
                                break;
                            }
                            sftpClient.Disconnect();
                            return "ERROR-404|" + str3 + path3;
                        }
                }
                str1 = DateTime.Now.ToFileTime().ToString() + ".csv";
                Console.WriteLine("File Received.");
                using (StreamWriter streamWriter = new StreamWriter("C:\\Importer Test\\" + str1))
                    streamWriter.Write(str2);
            }
            catch (Exception ex)
            {
                Program.sendDebugEmail("Import Application Runtime Error", ex);
                Console.Write(ex.Message);
                return str1;
            }
            return str1;
        }

        private static string GetIfExistWrapper(string function, string sql) => sql;

        private static FileResult processCSV(SignatureObject _sig, string filename = "")
        {
            string distinct = _sig.distinct;
            FileResult fileResult = new FileResult();
            if (filename.Substring(0, 2) != "C:")
                filename = "C:\\Importer Test\\" + filename;
            if (!System.IO.File.Exists(filename))
            {
                fileResult.count = -1;
                return fileResult;
            }
            using (FileStream fileStream = new FileStream(filename, FileMode.Open))
            {
                using (BufferedStream bufferedStream = new BufferedStream((Stream)fileStream))
                {
                    using (SHA1Managed shA1Managed = new SHA1Managed())
                    {
                        byte[] hash = shA1Managed.ComputeHash((Stream)bufferedStream);
                        StringBuilder stringBuilder = new StringBuilder(2 * hash.Length);
                        foreach (byte num in hash)
                            stringBuilder.AppendFormat("{0:X2}", (object)num);
                        Console.WriteLine("SHA1 CHECKSUM:" + (object)stringBuilder);
                        fileResult.checksum = stringBuilder.ToString();
                        string str1 = "";
                        using (SqlConnection connection = new SqlConnection(Program.getConnectionString(_sig)))
                        {
                            try
                            {
                                SqlCommand sqlCommand1 = new SqlCommand();
                                string cmdText = "SELECT * FROM universe_sync WHERE checksum='" + (object)stringBuilder + "' AND signature='" + _sig.LinkingName + "' ";
                                Console.WriteLine("Checking if file has been imported.");
                                str1 += "Checking if file has been imported.<br /><br />";
                                SqlCommand sqlCommand2 = new SqlCommand(cmdText, connection);
                                sqlCommand2.Connection.Open();
                                SqlDataReader sqlDataReader = sqlCommand2.ExecuteReader();
                                bool flag = false;
                                try
                                {
                                    while (sqlDataReader.Read())
                                    {
                                        Console.WriteLine(string.Format("{0}, {1}", sqlDataReader[0], sqlDataReader[1]));
                                        fileResult.count = (int)sqlDataReader["count"] + 1;
                                        flag = true;
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Program.sendDebugEmail("Import Application Runtime Error", ex);
                                }
                                finally
                                {
                                    sqlDataReader.Close();
                                }
                                SqlCommand sqlCommand3;
                                if (flag)
                                {
                                    sqlCommand3 = new SqlCommand("UPDATE universe_sync SET count = (count + 1) WHERE checksum='" + (object)stringBuilder + "' AND signature='" + _sig.LinkingName + "' ", connection);
                                    sqlCommand3.ExecuteNonQuery();
                                    Console.WriteLine("This file with checksum " + (object)stringBuilder + " has already been imported against signature " + _sig.LinkingName + " skipping import! ");
                                }
                                else
                                {
                                    sqlCommand3 = new SqlCommand("INSERT INTO universe_sync (checksum,signature,count)VALUES('" + (object)stringBuilder + "', '" + _sig.LinkingName + "', 1);", connection);
                                    sqlCommand3.ExecuteNonQuery();
                                    fileResult.count = 1;
                                }
                                sqlCommand3.Connection.Close();
                            }
                            catch (Exception ex)
                            {
                                Program.sendDebugEmail("Import Application Runtime Error", ex);
                                Console.WriteLine(ex.Message);
                                string str2 = str1 + "SQL Error Occured: " + ex.Message + "<br /><br />";
                            }
                        }
                    }
                }
            }
            string s = System.IO.File.ReadAllText(filename);
            string str3 = "";
            using (CsvReader csvReader = new CsvReader((TextReader)new StringReader(s), true))
            {
                int fieldCount = csvReader.FieldCount;
                string[] fieldHeaders = csvReader.GetFieldHeaders();
                string str1 = "INSERT INTO " + _sig.local_db_table + " ( ";
                int num1 = 0;
                foreach (string list in _sig.list_Map)
                {
                    string[] strArray1 = list.Split(':', ':');
                    if (strArray1.Length == 0)
                        strArray1[0] = list;
                    string[] strArray2 = strArray1[0].Split('=', '>');
                    string str2 = strArray2[0];
                    strArray2[2].Replace("'", "''");
                    Program.GetNumericHeader(fieldHeaders, strArray2[2]);
                    str1 = str1 + " " + str2 + " ";
                    if (num1 < _sig.list_Map.Length - 1)
                        str1 += ", ";
                    ++num1;
                }
                string str4 = str1 + " ) VALUES " + Environment.NewLine;
                int num2 = 0;
                int num3 = 0;
                ArrayList arrayList = new ArrayList();
                Hashtable hashtable = new Hashtable();
                while (csvReader.ReadNextRecord())
                {
                    if (distinct.Length > 0)
                    {
                        if (!hashtable.ContainsKey((object)csvReader[distinct]))
                            hashtable.Add((object)csvReader[distinct], (object)csvReader[distinct]);
                        else
                            continue;
                    }
                    ++num2;
                    string str2 = str3 + " ( ";
                    int num4 = 0;
                    foreach (string list in _sig.list_Map)
                    {
                        bool flag1 = false;
                        bool flag2 = false;
                        try
                        {
                            string[] strArray1 = list.Split(':', ':');
                            if (strArray1.Length > 1)
                            {
                                switch (strArray1[2])
                                {
                                    case "ToNumeric()":
                                        flag1 = true;
                                        break;
                                    case "ToEncrypt()":
                                        flag2 = true;
                                        break;
                                }
                            }
                            else
                                strArray1[0] = list;
                            string[] strArray2 = strArray1[0].Split('=', '>');
                            string str5 = strArray2[0];
                            strArray2[2].Replace("'", "''");
                            int numericHeader = Program.GetNumericHeader(fieldHeaders, strArray2[2]);
                            string str6 = "INSERT";
                            string str7 = !flag1 ? (!flag2 ? " '" + csvReader[numericHeader].Replace("'", "") + "' " : Program.simpleEncrypt(csvReader[numericHeader])) : " '" + Program.convertNumeric(csvReader[numericHeader]) + "' ";
                            if (str6 == "UPDATE")
                            {
                                if (num4 < _sig.list_Map.Length - 1)
                                    str2 = str2 + str5 + " = " + str7 + ",";
                                else
                                    str2 = str2 + str5 + " = " + str7 + " ";
                            }
                            else if (str6 == "INSERT")
                                str2 = num4 >= _sig.list_Map.Length - 1 ? str2 + " " + str7 + "  " : str2 + " " + str7 + " ,";
                        }
                        catch (Exception ex)
                        {
                            Program.sendDebugEmail("Import Application Runtime Error", ex);
                        }
                        ++num4;
                    }
                    string str8 = str2 + " ) ";
                    if (num2 % 500 == 0)
                    {
                        string str5 = str4 + str8;
                        arrayList.Add((object)str5);
                        str3 = "";
                        ++num3;
                    }
                    else
                        str3 = str8 + ", " + Environment.NewLine;
                }
                string str9 = str4 + str3;
                if (str9.Replace(",", "").Replace(Environment.NewLine, "").Trim().Length != 0)
                    arrayList.Add((object)str9);
                fileResult.Transactions = arrayList;
                return fileResult;
            }
        }

        private static void sendEmailCritical(SignatureObject _sig, string subject, string msg)
        {
            string address = "notification@dirad.com";

            MailMessage mailMsg = new MailMessage();
            foreach (string onCriticalEmail in _sig.onCriticalEmails)
            {
                mailMsg.To.Add(new MailAddress(onCriticalEmail));

            }

            mailMsg.From = new MailAddress(address);
            mailMsg.Subject = subject;
            mailMsg.Body = msg;
            mailMsg.IsBodyHtml = true;

            SmtpClient smtpClient = new SmtpClient("smtp.sendgrid.net", Convert.ToInt32(587));
            smtpClient.Credentials = new System.Net.NetworkCredential("apikey", "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");
            smtpClient.Send(mailMsg);
        }

        private static void sendEmailError(SignatureObject _sig, string subject, string msg)
        {
            msg = "Version:" + Program.ApplicationVersion + "\n\n   " + msg;
            string address = "notification@dirad.com";
            MailMessage mailMsg = new MailMessage();
            foreach (string onGoodEmail in _sig.onGoodEmails)
            {
                mailMsg.To.Add(new MailAddress(onGoodEmail));

            }

            mailMsg.From = new MailAddress(address);
            mailMsg.Subject = subject;
            mailMsg.Body = msg;
            mailMsg.IsBodyHtml = true;

            SmtpClient smtpClient = new SmtpClient("smtp.sendgrid.net", Convert.ToInt32(587));
            smtpClient.Credentials = new System.Net.NetworkCredential("apikey", "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");
            smtpClient.Send(mailMsg);
        }

        private static void sendDebugEmail(string subject, Exception ex)
        {

        }

        private static void  sendEmail(SignatureObject _sig, string subject, string msg)
        {
            Console.WriteLine("Sending Email");
            msg = "Version:" + Program.ApplicationVersion + "\n\n   " + msg;
    
            string address = "notification@dirad.com";
            string to = "";
            MailMessage mailMsg = new MailMessage();
            foreach (string onGoodEmail in _sig.onGoodEmails)
            {
                mailMsg.To.Add(new MailAddress(onGoodEmail));
                Console.WriteLine("To: " + onGoodEmail);
            }
            

            mailMsg.From = new MailAddress(address);
            mailMsg.Subject = subject;
            mailMsg.Body = msg;
            mailMsg.IsBodyHtml = true;

            SmtpClient smtpClient = new SmtpClient("smtp.sendgrid.net", Convert.ToInt32(587));
            smtpClient.Credentials = new System.Net.NetworkCredential("apikey", "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");
            try
            {
                smtpClient.Send(mailMsg);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            return ;
        }

        private static string getConnectionString(SignatureObject _sig)
        {

           // if (_sig.local_db_use_windows_auth)
          //  {
            //Console.WriteLine("Server=" + _sig.local_db_server + ";Initial Catalog=" + _sig.local_db_database + ";Integrated Security=SSPI;");
            //    return "Server=" + _sig.local_db_server + ";Initial Catalog=" + _sig.local_db_database + ";Integrated Security=SSPI;";
           // }
            //else {
                Console.WriteLine("Server=" + _sig.local_db_server + ";Initial Catalog=" + _sig.local_db_database + ";User Id=" + _sig.local_db_user + "; Password=" + _sig.local_db_pass + ";");
                return "Server=" + _sig.local_db_server + ";Initial Catalog=" + _sig.local_db_database + ";User Id=" + _sig.local_db_user + "; Password=" + _sig.local_db_pass + ";";
            //}
        }

        private static void Main(string[] args)
        {
            string shortTimeString = DateTime.Now.ToShortTimeString();
            string str1 = "";
            DateTime now1 = DateTime.Now;
            int num1 = 0;
            int num2 = 0;
            bool flag1 = false;
            if (flag1)
                args = "C:\\DiRAD Files\\Volusia Correctional\\Importer Signatures\\SCOTT___TESTFILE.sig".Split('|');
            foreach (string str2 in args)
            {
                try
                {
                    if (System.IO.File.Exists(str2))
                    {
                        SignatureObject signatureObject = new SignatureObject();
                        Serializer serializer = new Serializer();
                        ArrayList arrayList = new ArrayList();
                        SignatureObject _sig = serializer.DeSerializeObject(str2);
                        string str3 = "";
                        string[] strArray = new string[0];
                        switch (_sig.SigType.ToUpper())
                        {
                            case "MIRROR-FOLDER2FTP":
                                string str4 = str3 + "MIRROR-FOLDER2FTP SIGNATURE: " + _sig.LinkingName + "<br />" + "Source Directory:" + _sig.source_dir + "<br />" + "Destination: FTP <br />" + "   Host: " + _sig.ftp_host + "<br />" + "   Working Directory: " + _sig.ftp_working_dir + "<br />" + "   Username: " + _sig.ftp_user + "<br />" + "   Password: ********" + "<br />    Mode: Ascii" + "<br />Process Started: " + shortTimeString + "<br /><br />" + "************** RESULTS ******************* <br />";
                                try
                                {
                                    System.Net.FtpClient.FtpClient ftpClient = new System.Net.FtpClient.FtpClient();
                                    string[] files = Directory.GetFiles(_sig.source_dir);
                                    string ftpHost = _sig.ftp_host;
                                    string ftpUser = _sig.ftp_user;
                                    string ftpPass = _sig.ftp_pass;
                                    string path1 = _sig.ftp_working_dir;
                                    Console.WriteLine("Getting File List From Directory:");
                                    Console.WriteLine("FTP Working Directory:" + path1);
                                    str4 += "Getting Source File List <br />";
                                    foreach (string str5 in files)
                                        Console.WriteLine(str5);
                                    ftpClient.Credentials = new NetworkCredential()
                                    {
                                        UserName = ftpUser,
                                        Password = ftpPass
                                    };
                                    ftpClient.Host = ftpHost;
                                    ftpClient.Connect();
                                    ftpClient.SetWorkingDirectory(path1);
                                    str4 += "Connecting to remote host <br />";
                                    if (ftpClient.IsConnected)
                                    {
                                        str4 += "FTP is Connected. <br />";
                                        Console.WriteLine("FTP is Connected.");
                                    }
                                    else
                                    {
                                        str4 += "FTP is NOT Connected - Error Occured. <br />";
                                        Console.WriteLine("FTP is not Connected.");
                                    }
                                    FtpListItem[] listing = ftpClient.GetListing();
                                    str4 = str4 + "<br /><br />Retrieved remote directory listing for working directory " + _sig.ftp_working_dir + " <br />";
                                    Console.WriteLine();
                                    Console.WriteLine("Remote Directory Listing (FTP):");
                                    foreach (FtpListItem ftpListItem in listing)
                                        ;
                                    Console.WriteLine("GATHERING FILES NOT IN SYNC");
                                    str4 += "<br />Comparing Folders... <br />";
                                    foreach (string str5 in files)
                                    {
                                        Console.WriteLine("we have source files");
                                        string str6 = str5.Split('\\')[str5.Split('\\').Length - 1];
                                        bool flag2 = false;
                                        foreach (FtpListItem ftpListItem in listing)
                                        {
                                            if (str6 == ftpListItem.Name)
                                                flag2 = true;
                                        }
                                        if (!flag2)
                                            arrayList.Add((object)str5);
                                    }
                                    if (arrayList.Count == 0)
                                    {
                                        ftpClient.Disconnect();
                                    }
                                    else
                                    {
                                        Console.WriteLine("The following files need to be synced.");
                                        int num3 = 0;
                                        strArray = (string[])arrayList.ToArray(typeof(string));
                                        foreach (string path2 in arrayList)
                                        {
                                            ++num3;
                                            string path3 = path2.Split('\\')[path2.Split('\\').Length - 1];
                                            Console.Clear();
                                            Console.WriteLine();
                                            Console.WriteLine("Processing " + (object)num3 + " of " + (object)arrayList.Count);
                                            Console.WriteLine("Attempting To Transfer:");
                                            Console.WriteLine("     " + path3);
                                            str4 = str4 + "<br />Transfering " + path3 + " <br /><br />";
                                            try
                                            {
                                                Stream stream = ftpClient.OpenWrite(path3);
                                                FileStream fileStream = new FileStream(path2, FileMode.Open, FileAccess.Read);
                                                try
                                                {
                                                    byte[] buffer = new byte[8192];
                                                    double num4 = 0.0;
                                                    int count;
                                                    while ((count = fileStream.Read(buffer, 0, buffer.Length)) > 0)
                                                    {
                                                        stream.Write(buffer, 0, count);
                                                        num4 += (double)count;
                                                        double num5 = num4 / (double)fileStream.Length * 100.0;
                                                        Console.Clear();
                                                        Console.WriteLine();
                                                        Console.WriteLine("Processing " + (object)num3 + " of " + (object)arrayList.Count);
                                                        Console.WriteLine("Attempting To Transfer:");
                                                        Console.WriteLine("     " + path3);
                                                        Console.WriteLine();
                                                        Console.WriteLine("Uploaded " + (object)Math.Round(num5, 2) + " %  " + (object)num4 + " of " + (object)fileStream.Length);
                                                        str4 = str4 + "Uploaded " + (object)Math.Round(num5, 2) + " %  " + (object)num4 + " of " + (object)fileStream.Length + " <br />";
                                                    }
                                                    if (!Directory.Exists(_sig.source_dir + "\\processed\\" + path3))
                                                        Directory.CreateDirectory(_sig.source_dir + "\\processed\\");
                                                }
                                                finally
                                                {
                                                    fileStream.Close();
                                                    stream.Close();
                                                    ftpClient.Disconnect();
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                Program.sendDebugEmail("Import Application Runtime Error", ex);
                                                Console.WriteLine("An error occured during the transfer " + ex.Message);
                                                str4 = str4 + "An error occured during the transfer " + ex.Message;
                                            }
                                            Console.WriteLine("Moving File Now");
                                            System.IO.File.Move(_sig.source_dir + "\\" + path3, _sig.source_dir + "\\processed\\" + path3);
                                            str4 = str4 + "<br /> Moved file " + path3 + " to \"processed\" folder. ";
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Program.sendDebugEmail("Import Application Runtime Error", ex);
                                    Console.WriteLine(ex.Message);
                                }
                                string msg1 = str4 + "Transfers Complete!";
                                if (arrayList.Count > 0)
                                {
                                    Program.sendEmail(_sig, "MIRROR-FOLDER2FTP", msg1);
                                    break;
                                }
                                break;
                            case "CSV":
                                string filename = Program.DownloadFile(_sig).ToString();
                                str1 = filename;
                                bool flag3 = false;
                                switch (filename.Split('|')[0])
                                {
                                    case "NO-FILE-EXIT":
                                        Console.WriteLine("No File Found, Exiting!.");
                                        continue;
                                    case "ERROR-404":
                                        Console.WriteLine("The file does not exist, using last good file! " + filename.Split('|')[1]);
                                        filename = filename.Split('|')[1];
                                        flag3 = true;
                                        break;
                                    case "ERROR":
                                        Console.WriteLine(filename.Split('|')[1]);
                                        Console.WriteLine("connection error occurred, aborting..");
                                        Program.sendEmailCritical(_sig, "<!> S/FTP CONN FAILURE!", "The signature below failed connecting.. " + _sig.LinkingName + " => " + _sig.CSV_FILE_PATH_URL + " ATTEMPT ABORTED!");
                                        continue;
                                }
                                Console.WriteLine();
                                Console.WriteLine();
                                string str7 = str3 + "CSV IMPORTER SIGNATURE: " + _sig.LinkingName + "<br />" + "Source:" + _sig.CSV_FILE_PATH_URL + "<br />" + "Destination: Database<br />" + "    DB Name: " + _sig.local_db_database + "<br />" + "    Table: " + _sig.local_db_database + "<br />" + "   Username: " + _sig.local_db_user + "<br />" + "   Password: ********" + "   Win Auth: " + (object)_sig.local_db_use_windows_auth + "<br />" + "   Distinct: " + _sig.distinct + "<br />" + "    Archive:  Keeping " + _sig.ArchiveFileNumDays.ToString() + " Files.<br />" + "Process Started: " + shortTimeString + "<br />";
                                Console.WriteLine(str7.Replace("<br />", Environment.NewLine));
                                DateTime now2 = DateTime.Now;
                                string str8 = str7 + "Process Completed: " + now2.ToShortTimeString() + "<br /><br />";
                                Console.WriteLine("******************** PROCESSING NOW ********************");
                                string str9 = str8 + "******************** PROCESSING RESULTS ********************<br /><br />";
                                bool flag4 = true;
                                FileResult fileResult = Program.processCSV(_sig, filename);
                                if (fileResult.count > 1)
                                {
                                    if (flag3)
                                        continue;
                                }
                                if (fileResult.count != -1)
                                {
                                    int num3 = (fileResult.Transactions.Count - 1) * 500 + (fileResult.Transactions[fileResult.Transactions.Count - 1].ToString().Split('\n').Length - 1 - 1);
                                    num2 = num3;
                                    string msg2 = str9 + "      File Thumbprint: " + fileResult.checksum + "<br />" + "         Import Count: " + (object)fileResult.count + "<br />" + "        Total Records: " + (object)num3 + "<br />" + "    Transaction Count: " + (object)fileResult.Transactions.Count + "<br />" + " Transaction Max Size: 500 INSERTS/UPDATES<br />" + "***** SQL RESULTS *****<br /><br />";
                                    Console.WriteLine("Starting SQL Execution... " + num3.ToString() + " transactions!");
                                    string str5 = !flag4 ? _sig.LinkingName + " - Import Failed" : _sig.LinkingName + " - Import Successful";
                                    _sig.local_db_use_windows_auth = true;
                                    using (SqlConnection connection = new SqlConnection(Program.getConnectionString(_sig)))
                                    {
                                        SqlCommand sqlCommand = new SqlCommand();
                                        connection.Open();
                                        try
                                        {
                                            int num4 = 1;
                                            Console.WriteLine("BEGIN TRANSACTION -- Clearing Existing Records!");
                                            msg2 += "Clearing Existing Data!!<br /><br />";
                                            sqlCommand = new SqlCommand(" BEGIN TRANSACTION ", connection);
                                            sqlCommand.ExecuteNonQuery();
                                            sqlCommand = new SqlCommand("DELETE FROM " + _sig.local_db_table, connection);
                                            sqlCommand.ExecuteNonQuery();
                                            foreach (string transaction in fileResult.Transactions)
                                            {
                                                if (sqlCommand.Connection.State != ConnectionState.Open)
                                                    sqlCommand.Connection.Open();
                                                string cmdText = transaction.Trim();
                                                if (cmdText.Substring(cmdText.Length - 1) == ",")
                                                    cmdText = transaction.Substring(0, cmdText.Length - 2);
                                                Console.WriteLine("Executing Transaction " + (object)num4);
                                                msg2 = msg2 + "Executing Transaction " + (object)num4 + "<br /><br />";
                                                ++num4;
                                                using (StreamWriter streamWriter = new StreamWriter("C:\\Importer Test\\sql_out__" + num4.ToString() + ".sql"))
                                                    streamWriter.Write(cmdText);
                                                sqlCommand = new SqlCommand(cmdText, connection);
                                                sqlCommand.ExecuteNonQuery();
                                            }
                                            sqlCommand = new SqlCommand(" COMMIT ", connection);
                                            sqlCommand.ExecuteNonQuery();
                                        }
                                        catch (Exception ex)
                                        {
                                            Program.sendDebugEmail("SQL Error Rollback -- Import Application Runtime Error", ex);
                                            sqlCommand = new SqlCommand(" ROLLBACK ", connection);
                                            sqlCommand.ExecuteNonQuery();
                                            Console.WriteLine(ex.Message);
                                            msg2 = msg2 + "SQL Error Occured: " + ex.Message + "<br /><br />";
                                            ++num1;
                                            msg2 += "SQL Execution has errored, Rolled back.";
                                            ++num1;
                                        }
                                        finally
                                        {
                                            sqlCommand.Connection.Close();
                                        }
                                    }
                                    Console.WriteLine("SQL Execution was completed!");
                                    if (fileResult.count >= 5)
                                    {
                                        str5 = "Import Error (Same File >= 5 attempts) -" + str5;
                                        ++num1;
                                    }
                                    Program.sendEmail(_sig, str5 ?? "", msg2);
                                    break;
                                }
                                continue;
                        }
                        DateTime now3 = DateTime.Now;
                        Console.WriteLine("Attemplting to process trigger");
                        if (_sig.triggerDll.Trim().Length > 0)
                        {
                            if (System.IO.File.Exists(_sig.triggerDll.Trim()))
                            {
                                switch (_sig.SigType.Trim().ToUpper())
                                {
                                    case "MIRROR-FOLDER2FTP":
                                        object[] args1 = new object[1]
                                        {
                      (object) strArray
                                        };
                                        DynaInvoke.InvokeMethod(_sig.triggerDll, _sig.on_success_class, _sig.on_success_method, args1);
                                        continue;
                                    case "CSV":
                                        object[] args2 = new object[6]
                                        {
                      (object) str1,
                      (object) now1,
                      (object) now3,
                      (object) num2,
                      (object) num1,
                      (object) Program.getConnectionString(_sig)
                                        };
                                        Console.WriteLine("Passing " + (object)args2.Length + " params");
                                        DynaInvoke.InvokeMethod(_sig.triggerDll, _sig.on_success_class, _sig.on_success_method, args2);
                                        continue;
                                    default:
                                        continue;
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Program.sendDebugEmail("Import Application Runtime Error", ex);
                    Console.WriteLine(ex.Message);
                }
            }
            if (!flag1)
                return;
            Console.ReadLine();
        }
    }
}